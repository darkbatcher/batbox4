/* Batbox - Fast tool for displaying console graphics
   Copyright (C) 2017 Romain GARBI

   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions are met:

      1. Redistributions of source code must retain the above copyright notice,
         this list of conditions and the following disclaimer.

      2. Redistributions in binary form must reproduce the above copyright
         notice, this list of conditions and the following disclaimer in the
         documentation and/or other materials provided with the distribution.

      3. Neither the name of the copyright holder nor the names of its
         contributors may be used to endorse or promote products derived from
         this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
*/
#include <stdio.h>
#include <stdlib.h>
#include <libDos9.h>

int start_background(char** argv)
{
#if defined(BB_STANDALONE)
    int pid = GetProcessId(parent);
    ESTR* command = Dos9_EsInit();
    char path[FILENAME_MAX], tmp[15];
    STARTUPINFO info={sizeof(info)};
    PROCESS_INFORMATION process;

    /* deal with the executable name */
    GetModuleFileName(NULL, path, sizeof(path));
    Dos9_EsCpy(command, "\"");
    Dos9_EsCat(command, path);
    Dos9_EsCat(command, "\" ");

    /* deal with parent pid nb */
    snprintf(tmp, sizeof(tmp), "%u ", pid);
    Dos9_EsCat(command, tmp);

    /* Generate the command line */
    while (*argv) {
        Dos9_EsCat(command, "\"");
        Dos9_EsCat(command, *argv);
        Dos9_EsCat(command, "\" ");
        argv ++;
    }

    if (!CreateProcess(path, command->str, NULL, NULL, FALSE,
                                        0, NULL, NULL, &info, &process)) {

        fprintf(stderr, "batbox: Unable to start background process\n");
        exit(-1);

    }

    CloseHandle(process.hProcess);
    CloseHandle(process.hThread);

    return 0;
#else
    /* This is not the standalone version of batbox...
       only launch a thread */
#endif /* BB_STANDALONE */
}

