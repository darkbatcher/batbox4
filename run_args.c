/* Batbox - Fast tool for displaying console graphics
   Copyright (C) 2017 Romain GARBI

   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions are met:

      1. Redistributions of source code must retain the above copyright notice,
         this list of conditions and the following disclaimer.

      2. Redistributions in binary form must reproduce the above copyright
         notice, this list of conditions and the following disclaimer in the
         documentation and/or other materials provided with the distribution.

      3. Neither the name of the copyright holder nor the names of its
         contributors may be used to endorse or promote products derived from
         this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
*/
#include <stdio.h>
#include <stdlib.h>
#include <conio.h>

#include "inject_var.h"

#include "lock_parent.h"
#include "start_background.h"

int run_args(char** argv)
{
    char **orig = argv;

    while (*argv) {

        if (fast_switch_cmp(*argv, 'd')) {
            /* display an argument */

            if (*(++ argv))
                fputs(*argv, stdout);

        } else if (fast_switch_cmp(*argv, 'a')) {
            /* print an ascii character */

            if (*(++ argv))
                printf("%c", strtol(*argv, NULL, 0));

        } else if (fast_switch_cmp(*argv, 'k')
                   || fast_switch_cmp(*argv, 'l')) {

            int wait = 1,
                name = 0,
                key,
                type;
            char buf[FILENAME_MAX];

            if (**argv && *(*argv + 1) && (*(*argv + 2) == '_'))
                wait = 0;

            type = *(*argv+1);

            if (fast_noswitch_check(*(argv+1)) &&
                *(++ argv))
                    name = 1;

            if (!wait && !kbhit())
                continue;

            key = getch();

            /* If we got an extended key */
            if (key == 224)
                key += getch();

            if (name) {

                snprintf(buf, sizeof(buf), ((type == 'l' || type == 'L')
                                           && (key < 255)) ? "%c" : "%d", key);

                inject_var(*argv, buf);

            } else {

                /* return the key and exit */
                return key;

            }

        } else if (fast_switch_cmp(*argv, 'g')) {
            /* changes the console cursor position */

            CONSOLECOORD coord;

            if (*(++ argv)) {

                coord.X = strtol(*argv, NULL, 0);
                if (*(++ argv)) {
                    coord.Y = strtol(*argv, NULL, 0);
                    Dos9_SetConsoleCursorPosition(coord);
                }

            }

        } else if (fast_switch_cmp(*argv, 'c')) {
            /* change the console color */

            if (*(++ argv))
                Dos9_SetConsoleTextColor(strtol(*argv, NULL, 0));

        } else if (fast_switch_cmp(*argv, 'b')) {
            /* launch in background */

            if (*(++ argv))
                start_background(argv);

            return 0;

        } else if (fast_switch_cmp(*argv, 'j')) {
            /* execs a jump */
            argv = orig;
            continue;

        } else if (fast_switch_cmp(*argv, 'w')) {

            if (*(++ argv))
                    Sleep(strtol(*argv, NULL, 0));

        }

        if (*argv == NULL)
            break;

        argv ++;

    }

    return 0;
}
