/* Batbox - Fast tool for displaying console graphics
   Copyright (C) 2017 Romain GARBI

   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions are met:

      1. Redistributions of source code must retain the above copyright notice,
         this list of conditions and the following disclaimer.

      2. Redistributions in binary form must reproduce the above copyright
         notice, this list of conditions and the following disclaimer in the
         documentation and/or other materials provided with the distribution.

      3. Neither the name of the copyright holder nor the names of its
         contributors may be used to endorse or promote products derived from
         this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
//#include <libDos9.h>

#include "inject_var.h"
#include "run_args.h"

#if !defined(WIN32) && defined(BB_STANDALONE)
#error It is not possible to build a standalone version in platforms other than windows
#endif

void help(void)
{
    fprintf(stderr, "blablabla msg\n");
}

int assync = 0;

#if defined(BB_STANDALONE)
int main(int argc, char* argv[])
#else
int Dos9_RunBatbox(int argc, char* argv[])
#endif /* BB_STANDALONE */
{
    int ret, pid, i=1;

    if (argv[1] == NULL) {

        fprintf(stderr, "Error: Not enough arguments to Batbox\n");
        return -1;

    }

    if (fast_switch_cmp(argv[1], '?'))
        help();

    if (*(argv[1]) >= '0'
        && *(argv[1]) <= '9') {

#ifdef BB_STANDALONE
        /* the process id of the parent was given on the command line */
        pid = atoi(argv[1]);
        parent = OpenProcess(PROCESS_ALL_ACCESS, FALSE, pid);

        if (parent == NULL) {
            fprintf(stderr, "Error : Unable to get handle to process %d", pid);
            return -1;
        }

        assync = 1;
#endif /* BB_STANDALONE */

        i = 2;

    }

    if (init_inject_var()) {

        fprintf(stderr, "Error: Unable to setup injection stuff ...\n");
        return -1;

    }

    ret = run_args(&(argv[i]));

    free_inject_var();
    return ret;
}
