/* Batbox - Fast tool for displaying console graphics
   Copyright (C) 2017 Romain GARBI

   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions are met:

      1. Redistributions of source code must retain the above copyright notice,
         this list of conditions and the following disclaimer.

      2. Redistributions in binary form must reproduce the above copyright
         notice, this list of conditions and the following disclaimer in the
         documentation and/or other materials provided with the distribution.

      3. Neither the name of the copyright holder nor the names of its
         contributors may be used to endorse or promote products derived from
         this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
*/
#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <tlhelp32.h>
#include <assert.h>

struct var_data_t {
    char name[1024];
    char content[1024];
    BOOL WINAPI(*set_env_fn_pointer)(void*, void*);
};

WINAPI int(*remote_set_var)(struct var_data_t* data) = NULL;
WINAPI int(*remote_get_var)(struct var_data_t* data) = NULL;
struct var_data_t* remote_var_data = NULL;
HANDLE parent=NULL;

void free_remote_object(HANDLE h, void* obj)
{
    VirtualFreeEx(h, obj, 0, MEM_RELEASE);
}

int __declspec(dllexport) WINAPI to_inject_set_var(struct var_data_t* data)
{
    return data->set_env_fn_pointer(data->name, data->content);
}

int __declspec(dllexport) WINAPI to_inject_get_var(struct var_data_t* data)
{
    return ((BOOL WINAPI(*)(void*, void*, int))
            data->set_env_fn_pointer)(data->name, data->content, 1024);
}

HANDLE get_parent_handle(void)
{
    int pid = -1;
    HANDLE ret=NULL;
    HANDLE h = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
    PROCESSENTRY32 pe = { 0 };

    pe.dwSize = sizeof(PROCESSENTRY32);

    pid = GetCurrentProcessId();

    if( Process32First(h, &pe)) {
        do {
            if (pe.th32ProcessID == pid) {
                ret = OpenProcess(PROCESS_ALL_ACCESS, FALSE, pe.th32ParentProcessID);
            }
        } while( Process32Next(h, &pe));
    }

    CloseHandle(h);
    return ret;
}

size_t get_sys_page_size (void)
{
    SYSTEM_INFO sysinfo;
    GetSystemInfo(&sysinfo);

    return sysinfo.dwPageSize;
}

void* inject_remote_object(HANDLE h, void* obj, size_t size)
{
    void* remote;
    SIZE_T written;

    remote = VirtualAllocEx(h, NULL, size, MEM_RESERVE | MEM_COMMIT,
                                            PAGE_EXECUTE_READWRITE);

    if (remote == NULL)
        return NULL;

    if (!WriteProcessMemory(h, remote, obj, size, &written)
        || written != size) {
        VirtualFreeEx(h, remote, 0, MEM_RELEASE);
        return NULL;
    }

    return remote;
}

int write_remote_object(HANDLE h, void* remote, void* obj, size_t size)
{
    SIZE_T written;

    if (!WriteProcessMemory(h, remote, obj, size, &written)
        || written != size)
        return 1;

    return 0;
}

int read_remote_object(HANDLE h, void* remote, void* obj, size_t size)
{
    SIZE_T written;

    if (!ReadProcessMemory(h, remote, obj, size, &written)
        || written != size)
        return 1;

    return 0;
}


int run_remote_thread(HANDLE h, void* fn, void* param)
{
    HANDLE handle;

    handle = CreateRemoteThread(h, NULL, 0, fn, param, 0, NULL);

    if (handle == NULL)
        return 1;

    WaitForSingleObject(handle, INFINITE);

    CloseHandle(handle);
    return 0;
}

int init_inject_var(void)
{
    BOOL wow64;
    size_t fnsize,
            fnsize2,
            pagesize;
    int i = 0;

    struct var_data_t var;

    /* include the to_inject_functions_sizes[] array generated from the
       executable and symbol-size-extract script at runtime */
    #include "symbol_sizes.c"

    if (parent == NULL)
        parent = get_parent_handle();

    if (parent == NULL)
        return -1;

    IsWow64Process(parent, &wow64);

    if (wow64 != 0)
        return -1;

    /* Retrieve size of to_inject_set_var() and to_inject_get_var()
       functions in memory. This actually work because there is
       a script calculating sizes of functions from the executable
       named symbol-size-extract */

    while (to_inject_functions_sizes[i].fn) {

        if (to_inject_functions_sizes[i].fn == to_inject_get_var)
                fnsize2 = to_inject_functions_sizes[i].size;
        else if (to_inject_functions_sizes[i].fn == to_inject_set_var)
                fnsize = to_inject_functions_sizes[i].size;

        i++;

    }

    pagesize = get_sys_page_size();
    remote_set_var = NULL;

    remote_set_var =
            inject_remote_object(parent, to_inject_set_var, fnsize);

    if (remote_set_var == NULL)
        return -1;

    if (fnsize + sizeof(struct var_data_t) > pagesize) {

        remote_var_data = inject_remote_object(parent, &var, sizeof(var));

        if (remote_var_data == NULL)
            return -1;

    } else {

        remote_var_data = (void*)remote_set_var + fnsize;

    }

    fnsize += sizeof(struct var_data_t);

    if (fnsize + fnsize2 > pagesize) {

        remote_get_var =
            inject_remote_object(parent, to_inject_get_var, fnsize2);

        if (remote_set_var == NULL)
            return -1;

    } else {

        remote_get_var = (void*)remote_set_var + fnsize;
        if (write_remote_object(parent, remote_get_var,
                                to_inject_get_var, fnsize2))
            return -1;

    }

    return 0;
}

int inject_var(const char* name, const char* value)
{
    struct var_data_t data;

    strncpy(data.name, name, sizeof(data.name));
    data.name[sizeof(data.name) -1] = '\0';
    strncpy(data.content, value, sizeof(data.name));
    data.content[sizeof(data.content) -1] = '\0';

    data.set_env_fn_pointer = SetEnvironmentVariableA;

    if (write_remote_object(parent,
                        remote_var_data, &data, sizeof(data)))
        return -1;

    if (run_remote_thread(parent, remote_set_var, remote_var_data))
        return -1;

    return 0;
}

int retrieve_var(const char* name, char* buf, size_t size)
{
    struct var_data_t data;

    strncpy(data.name, name, sizeof(data.name));
    data.name[sizeof(data.name) -1] = '\0';

    data.set_env_fn_pointer = GetEnvironmentVariableA;

    if (write_remote_object(parent,
                        remote_var_data, &data, sizeof(data)))
        return -1;

    if (run_remote_thread(parent, remote_get_var, remote_var_data)
        || read_remote_object(parent, remote_var_data->content, &(data.content),
                              sizeof(data.content)))
        return -1;

    strncpy(buf, data.content, size);
    buf[size-1] = '\0';

    return 0;
}

int free_inject_var(void)
{
    /* I should do it more consciously but, there's arguably *no way* the inject
       function we sat up are using more than one page of memory */
    free_remote_object(parent, remote_set_var);
    CloseHandle(parent);
}

