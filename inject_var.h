/* Batbox - Fast tool for displaying console graphics
   Copyright (C) 2017 Romain GARBI

   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions are met:

      1. Redistributions of source code must retain the above copyright notice,
         this list of conditions and the following disclaimer.

      2. Redistributions in binary form must reproduce the above copyright
         notice, this list of conditions and the following disclaimer in the
         documentation and/or other materials provided with the distribution.

      3. Neither the name of the copyright holder nor the names of its
         contributors may be used to endorse or promote products derived from
         this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef BB_INJECT_VAR_H
#define BB_INJECT_VAR_H

#if defined(BB_STANDALONE) && !defined(__x86_64__)
#error Building standalone version of batbox needs use of x86_64 compiler
#endif /* defined(BB_STANDALONE) && !defined(__x86_64__) */

#if defined(BB_STANDALONE)

extern HANDLE parent;
int inject_var(const char* name, const char* value);
int retrieve_var(const char* name, char* buf, size_t size);
int init_inject_var(void);
int free_inject_var(void);

struct fn_size_t {
    size_t size;
    void* fn;
};

#include "inject_var.c"

#else

/* definitions from dos9_core.h */
typedef struct ENVBUF ENVBUF;
char* Dos9_GetEnv(ENVBUF* pEnv, const char* name);
void  Dos9_SetEnv(ENVBUF* pEnv, const char* name, const char* content);
ENVBUF* lpeEnv;

#define inject_var(name, value) Dos9_SetEnv(pEnv, name, value);
#define retrieve_var(name, buf, size) \
    strncpy(buf, Dos9_GetEnv(lpeEnv, name) ? \
                        (Dos9_GetEnv(lpeEnv, name)) : "\0", size); \
    buf[size - 1] = '\0'
#define init_inject_var() 0 /* do nothing */
#define free_inject_var()

#endif /* BB_STANDALONE */

#endif /* BB_INJECT_VAR_H */
